-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `author`;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `author` (`id`, `name`) VALUES
(1,	'author1'),
(2,	'author2'),
(3,	'author3'),
(4,	'author4'),
(5,	'author5');

DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  `description` text NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `book` (`id`, `name`, `description`, `genre_id`) VALUES
(1,	'book1',	'text1',	1),
(2,	'book2',	'text2',	2),
(6,	'book5',	'text5',	5),
(7,	'book6',	'text6',	2),
(9,	'book100',	'text100',	5),
(11,	'book1',	'text1',	1),
(12,	'book1g',	'text1g',	1);

DROP TABLE IF EXISTS `book_author`;
CREATE TABLE `book_author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `book_author` (`id`, `book_id`, `author_id`) VALUES
(4,	1,	1),
(5,	1,	1),
(6,	2,	3),
(7,	4,	5),
(8,	4,	3),
(9,	6,	5);

DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `genre` (`id`, `name`) VALUES
(1,	'gener1'),
(2,	'gener2'),
(3,	'gener3'),
(4,	'gener4'),
(5,	'gener5');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` enum('user','admin') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`) VALUES
(9,	'kola',	'kola@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'user'),
(10,	'admin',	'admin@maill.com',	'e10adc3949ba59abbe56e057f20f883e',	'admin');

-- 2019-02-20 12:15:50
