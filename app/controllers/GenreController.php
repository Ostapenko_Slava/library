<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.07.18
 * Time: 20:40
 */

namespace app\controllers;


use app\models\Book;
use framework\base\Model;
use framework\App;

class GenreController extends AppController
{

    //public $layout = "test";

	public function indexAction()
	{

        $id = $_GET['id'];
		$book = new Book();
        $data = $book->getBooksByGenre($id);
        $books = [];
		foreach($data as $item){
			$books[$item['book']]['author'][] = $item['author'];
			$books[$item['book']]['description'] = $item['description'];

		}

        $this->set(compact("books"));
    }



}
