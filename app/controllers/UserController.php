<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 20.07.18
 * Time: 21:00
 */

namespace app\controllers;


use app\models\Article;
use app\models\User;

class UserController extends AppController
{
	public function registrationAction()
	{
		if (!empty($_POST)) {
			$user = new User();
			$data = $_POST;
			if (!$user->validate($data) || !$user->checkUnique($data['useremail'])) {
				$_SESSION['errors'] = $user->getErrors();
				$_SESSION['form_data'] = $data;
			} else {
				//hesh password and write data base
				//$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
				$data['password'] = md5($data['password']);
				$user->save($data);
				$_SESSION['success'] = 'пользователь зарегистрирован ';
			}
		}
		$this->setMeta("Регистрация", 'описание регистрации', "ключи");
	}

	public function loginAction()
	{
		if (!empty($_POST)) {
			$user = new User();
			$data = $_POST;
			//$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
			$data['password'] = md5($data['password']);
			if ($user->checkUser($data['useremail'], $data['password'])) {
				redirect("/user/cabinet");
			} else {
				$_SESSION['errors'] = $user->getErrors();
			}
		}
		$this->setMeta('login', 'page', 'key');
	}

	public function cabinetAction()
	{
		if(!User::auth()){////3
			redirect('/user/login');
		}
		$article = new Article();
		$data = $article->getArticleByUserId();
		$this->setMeta('cabinet', 'page', 'key');
		$this->set(compact('data'));
	}

	public function logoutAction()
	{
		session_destroy();
		redirect(PATH);
	}


}