<?php

/**
 * Created by PhpStorm.
 * User: slava
 * Date: 06.07.18
 * Time: 20:40
 */

namespace app\controllers;


use app\models\Book;
use framework\base\Model;
use framework\App;

class MainController extends AppController
{

    //public $layout = "test";

	public function indexAction()
	{

        $book = new Book();
		//$book->save();
        $data = $book->getBooks();
        $books = [];
		foreach($data as $item){
			$books[$item['book']]['author'][] = $item['author'];
			$books[$item['book']]['description'] = $item['description'];

		}
		$article = new Book();
		//$data = $article->getBook($id);
		//debug($data);
        //die();
        $this->set(compact("books"));
    }
}
