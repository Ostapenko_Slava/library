<?php


namespace app\controllers\admin;
use app\models\Book;
use app\models\Author;
use app\models\Genre;
use framework\base\Model;
use framework\App;

class MainController extends AppController
{

    public function indexAction()
     {
      $book = new Book();
      $data = $book->getAdminBooks();
      $this->set(compact("data"));
    }
	
	public function addAction()
    {
		if (isset($_POST['add_book'])){
			$data = $_POST;
			$book = new Book();
            $book->save($data);
		}
        $modelAuthor = new Author();
        $authors = $modelAuthor->getAuthors();

        $modelGenre = new Genre();
        $genres = $modelGenre->getGenres();
        
        $this->set(compact('authors','genres'));
    }
    public function showAction()
    {
        $id = (int) $_GET['id'];
        if (isset($_POST['edit_book'])) {
			$data = $_POST;
            $book = new Book();
            $book->update($data);
        }
		$book = [];
        $modelBook = new Book();
        $data = $modelBook->getBook($id);
		//debug($data);
		foreach($data as $item){
			$book['author_id'][] = $item['author_id'];
			$book['description'] = $item['description'];
			$book['name'] = $item['book'];
			$book['genre_id'] = $item['genre_id'];

		}
        $modelAuthor = new Author();
        $authors = $modelAuthor->getAuthors();

        $modelGenre = new Genre();
        $genres = $modelGenre->getGenres();
        $this->set(compact('book', 'authors','genres', 'id'));
    }
	
    public function deleteAction() {
      $id = (int) $_GET['id'];
      $book = new Book();
      $book->delete($id);
      redirect();
  }


}
