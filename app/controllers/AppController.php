<?php

namespace app\controllers;

use app\models\Author;
use app\models\Genre;
use framework\base\Controller;
use framework\App;
use framework\Cache;

class AppController extends Controller
{
    public function __construct($route)
    {
        parent::__construct($route);
        $this->setMeta(App::$app->getProperty("title"), App::$app->getProperty("description"), App::$app->getProperty("keywords"));
        $authors = $this->getListAuthors();
        App::$app->setProperty("authors",$authors);
		$genres = $this->getListGenres();
        App::$app->setProperty("genres",$genres);
    }

    //списка authors
    public function getListAuthors()
    {
        $cache = Cache::instance();
        $data = $cache->get("authors");
        if(!$data){
            $author = new Author();
            $data= $author->getAuthors();
            $cache->set("authors",$data);
        }
        return $data;
    }
	
	//списка genres
    public function getListGenres()
    {
        $cache = Cache::instance();
        $data = $cache->get("genres");
        if(!$data){
            $genre = new Genre();
            $data= $genre->getGenres();
            $cache->set("genres",$data);
        }
        return $data;
    }
}