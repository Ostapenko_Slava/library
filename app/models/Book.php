<?php

namespace app\models;

use framework\base\Model;
use \framework\libs\Pagination;

class Book extends Model
{


    public function getBooks()
	{

        $query = "SELECT author.name as author,book.name as book, description
			FROM author
				INNER JOIN book_author ON book_author.author_id=author.id
				INNER JOIN book ON book_author.book_id=book.id";
        $data = $this->allRows($query);
        return $data;
    }
	
	public function getAdminBooks()
	{

        $query = "SELECT id, name, description FROM book";
        $data = $this->allRows($query);
        return $data;
    }
	
    public function getBook($id)
  {
        $query = "SELECT author.id as author_id,book.name as book, description, book.genre_id 
			 FROM author
				INNER JOIN book_author ON book_author.author_id=author.id
				INNER JOIN book ON book_author.book_id=book.id WHERE book.id= ?";
        $data = $this->allRows($query, [$id]);
        return $data;
    }

	public function getBooksByGenre($id)
	{
        $id = (int)$id;
        $query = "SELECT author.name as author,book.name as book, description
			FROM author
				INNER JOIN book_author ON book_author.author_id=author.id
				INNER JOIN book ON book_author.book_id=book.id
			WHERE genre_id=$id";
        $data = $this->allRows($query);
        return $data;
    }

	public function getBooksByAuthor($id)
	{
        $id = (int)$id;
        $query = "SELECT author.name as author,book.name as book, description
			FROM author
				INNER JOIN book_author ON book_author.author_id=author.id
				INNER JOIN book ON book_author.book_id=book.id
			WHERE author.id=$id";
        $data = $this->allRows($query);
        return $data;
    }

	public function save($data) {

		$query = "INSERT INTO  book (name, description, genre_id) VALUE (?,?,?)";

        $lastId = $this->lastInsertRow($query, [
			       $data['name'],
			       $data['description'],
             $data['genre_id']
        ]);

		foreach($data['author_id'] as $author){
			$query = "INSERT INTO  book_author (book_id, author_id) VALUE (?,?)";
			$result = $this->insertRow($query, [
				$lastId,
				$author
            ]);
		}

        return $result;
    }
	
	public function update($data) {

		$id = $data['book_id'];
		$query = "UPDATE  book SET name=?, description=?, genre_id=? WHERE id=? LIMIT 1";

        $this->updateRow($query, [
			       $data['name'],
			       $data['description'],
             $data['genre_id'],
			 $id
        ]);

		$query = "DELETE FROM book_author WHERE book_id=?";
		$result = $this->deleteRow($query, [$id]);
		
		foreach($data['author_id'] as $author){
			$query = "INSERT INTO  book_author (book_id, author_id) VALUE (?,?)";
			$result = $this->insertRow($query, [
				$id,
				$author
            ]);
		}

        return $result;
    }
    public function delete($id)
     {
        $query = "DELETE FROM book WHERE id=? LIMIT 1";
        $result = $this->deleteRow($query, [$id]);
		$query = "DELETE FROM book_author WHERE book_id=?";
		$result = $this->deleteRow($query, [$id]);
        return true;
    }

    public function saveGenre($id) {
              $query = "DELETE FROM book WHERE id=? LIMIT 1";
              $result = $this->deleteRow($query, [$id]);
          return true;
      }




}
