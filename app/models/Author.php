<?php
/**
 * Created by PhpStorm.
 * User: slava
 * Date: 20.07.18
 * Time: 15:34
 */

namespace app\models;


use framework\base\Model;

class Author extends Model
{
    public function getAuthors()
    {
        $query = "SELECT  id,name FROM author ORDER BY name ASC";
        $data = $this->allRows($query);
        return $data;
    }

    public function getCatById($id)
    {
        $query = "SELECT  id,name,title, description, keywords FROM categories WHERE id = ? LIMIT 1";
        $data = $this->onllyRow($query,[$id]);
        return $data;
    }
}