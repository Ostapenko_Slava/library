﻿﻿<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <?= $this->getMeta() ?>
    </head>
    <body>
        <header>
            <div class="center-block-main">
                <nav>
                    <ul class="menu">
                        <li><a href="<?= PATH ?>">Главная</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="center-block-main content">
            <main>
                <?= $content ?>
            </main>
            <aside>
                <!--<div class="widget">
                    <h2>Search</h2>
                    <form action="/main/search/" method="get">
                        <input name="search" type="search" class="search" placeholder="What are you looking for?">
                    </form>
                </div>-->
                <div class="widget">
                    <h2>Authors</h2>
                    <nav>
                        <ul>
                            <?php
                            $authors = \framework\App::$app->getProperty("authors");
                            foreach ($authors as $author):?>
                                <li><a href="/author/?id=<?= $author["id"] ?>"><?= $author["name"] ?></a></li>
<?php endforeach ?>
                        </ul>
                    </nav>
                </div>

				<div class="widget">
                    <h2>Genres</h2>
                    <nav>
                        <ul>
                            <?php
                            $genres = \framework\App::$app->getProperty("genres");
                            foreach ($genres as $genre):?>
                                <li><a href="/genre/?id=<?= $genre["id"] ?>"><?= $genre["name"] ?></a></li>
<?php endforeach ?>
                        </ul>
                    </nav>
                </div>

            </aside>
            <div class="clr"></div>

        </div>
        <footer>
            <div class="center-block-main">
                <a href="#"><img src="/images/logo-ftr.jpg" alt=""></a>
                <p>Copyright &copy; 2017 Blogin.com - All right reserved - Find more Templates</p>
            </div>
        </footer>
        <!--spinner-->
        <div id="spinner">
            <div class="loader"></div>
        </div>

        <!-- alert modal -->
        <div id="myModal" class="modal-alert">
            <!-- Modal content -->
            <div class="alert-content">
                <div class="alert-header">
                    <span class="alert-close" onclick="closeAlert()">&times;</span>
                    <h3>Внимание ошибка!</h3>
                </div>
                <div class="alert-body">
                    <p>Непредвиденная ошибка</p>
                </div>
            </div>
        </div>
        <script src="/js/jquery-1.11.0.min.js"></script>
        <script src="/js/myscript.js"></script>
        <script src="/js/main.js"></script>
        <?php if (DEBUG): ?>
            <div class="debuger">
                <?php
                debug(\framework\base\Model::debugger());
                ?>
            </div>
        <?php endif; ?>
    </body>
</html>
