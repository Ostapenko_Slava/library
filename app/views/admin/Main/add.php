<h1>Add book admin</h1>
<?php if (isset($_SESSION['errors']) and ! empty($_SESSION['errors'])): ?>
    <div style="background: red">
        <?php foreach ($_SESSION['errors'] as $error): ?>
            <p><?= $error ?></p>
        <?php
        endforeach;
        unset($_SESSION['errors']);
        ?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
    ?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
    <?php
    unset($_SESSION['success']);
    unset($_SESSION['errors']);
endif;
?>

    <form action="add" method="post">
        <p>имя<input type="text" name="name" value=""></p>
        <select name="author_id[]" multiple>
            <?php foreach ($authors as $author): ?>
                <option value="<?= $author['id'] ?>"><?= $author['name'] ?></option>
            <?php endforeach; ?>
        </select><br>

        <select name="genre_id">
            <?php foreach ($genres as $genre): ?>
                <option value="<?= $genre['id'] ?>"><?= $genre['name'] ?></option>
            <?php endforeach; ?>
        </select><br>
        
        
        <p>description<textarea name="description"></textarea></p>
        <input type="submit" value="отправить" name="add_book">
    </form>

