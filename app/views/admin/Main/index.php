<h1>welcom to admin book</h1>
<?php if (isset($_SESSION['errors'])): ?>
    <div style="background: red">
        <?php foreach ($_SESSION['errors'] as $error): ?>
            <p><?= $error ?></p>
        <?php
        endforeach;
        unset($_SESSION['errors']);
        ?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
    ?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
    <?php
    unset($_SESSION['success']);
    unset($_SESSION['form_data']);
endif;
?>
<?php if (!empty($data)): ?>
    <table border="1">
        <tr>
            <td>#</td>
            <td>name book </td>
            <td>delete</td>
        </tr>
    <?php foreach ($data as $book): ?>
            <tr>
                <td><?= $book['id'] ?></td>
                <td><a href="/admin/main/show/?id=<?= $book['id'] ?>"><?= $book['name'] ?></a></td>
                <td><?= $book['description'] ?></td>
                <td><a onclick="return deleteAction()" href="/admin/main/delete/?id=<?= $book['id'] ?>">delete book</a></td>
            </tr>
    <?php endforeach; ?>
    </table>
    <?php else: ?>
    <h2>none book</h2>
<?php endif; ?>
