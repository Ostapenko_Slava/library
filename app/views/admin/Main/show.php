<h1>edit book admin</h1>
<?php if (isset($_SESSION['errors']) and ! empty($_SESSION['errors'])): ?>
    <div style="background: red">
        <?php foreach ($_SESSION['errors'] as $error): ?>
            <p><?= $error ?></p>
        <?php
        endforeach;
        unset($_SESSION['errors']);
        ?>
    </div>
<?php endif; ?>
<?php
if (isset($_SESSION['success'])):
    ?>
    <div style="background: #58c93a">
        <p><?= $_SESSION['success'] ?></p>
    </div>
    <?php
    unset($_SESSION['success']);
    unset($_SESSION['errors']);
endif;
?>

    <form action="" method="post">
	<input type="hidden" name="book_id" value="<?=$id?>">
        <p>имя<input type="text" name="name" value="<?= $book['name'] ?>"></p>
        <select name="author_id[]" multiple>
            <?php foreach ($authors as $author): ?>
			<?php $sel = '';?>
				<?php foreach ($book['author_id'] as $val_key): ?>
				<?php if($author['id'] == $val_key){
					$sel = 'selected';
					break;
				}
				?>
				<?php endforeach; ?>
                <option value="<?= $author['id'] ?>" <?=$sel?>><?= $author['name'] ?></option>
            <?php endforeach; ?>
        </select>
		<br>

        <select name="genre_id">
            <?php foreach ($genres as $genre): ?>
                <option value="<?= $genre['id'] ?>" <?= $genre['id'] == $book['genre_id'] ? 'selected' : '' ?>><?= $genre['name'] ?></option>
            <?php endforeach; ?>
        </select>

        
        <p>description</p>
		<textarea name="description"><?= $book['description'] ?></textarea><br>
        <input type="submit" value="отправить" name="edit_book">
    </form>
