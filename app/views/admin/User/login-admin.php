<?php if (isset($_SESSION['errors'])): ?>
    <div class="alert alert-danger">
        <?= $_SESSION['errors'][0] ?>
    </div>

    <?php unset($_SESSION['errors']);
endif;
?>
<div class="container">
    <div style="margin: 100px auto; width: 400px; background: #e1e0e0" >
        <form action="/admin/user/login-admin" method="post">
            email<input type="email" name="login" class="form-control">
            password<input type="password" name="password" class="form-control">
            <button type="submit" class="btn btn-primary">enter to admin panel</button>
        </form>
    </div>
</div>
