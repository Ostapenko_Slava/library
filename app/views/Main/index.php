<?php if (!empty($books)): ?>
    <?php foreach ($books as $key=>$book): ?>
        <article>

            <h2><?=$key?></h2>
            <p class="about">author: <?= implode(',',$book['author']) ?>
                |
            <p>description: <?= $book['description'] ?></p>
        </article>
    <?php endforeach ?>
    <?php else:?>
    <p>Books на найдено</p>
<?php endif ?>
